<?php

header("Content-Type:application/json");
include('connection.php');

if (isset($_GET['product_id']) && $_GET['product_id']!="") {

	$product_id = $_GET['product_id'];

	$query = "SELECT * FROM `products` WHERE id=$product_id";
	$result = mysqli_query($con,$query);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);

	$productData['id'] = $row['id'];
	$productData['name'] = $row['name'];
	$productData['description'] = $row['description'];
	$productData['price'] = $row['price'];
	$productData['created'] = $row['created'];
	$productData['modified'] = $row['modified'];
	$productData['image'] = $row['image'];

	$response["status"] = "true";
	$response["message"] = "Product Details";
	$response["products"] = $productData;

} else {
	$response["status"] = "false";
	$response["message"] = "No product(s) found!";
}
echo json_encode($response); exit;

?>