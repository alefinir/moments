<?php
        // get passed parameter value, in this case, the record ID
        // isset() is a PHP function used to verify if a value is there or not
        $id=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');
        
        //include database connection
        include '../config/database.php';
        
        // read current record's data
        try {
            // prepare select query
            //$query = "SELECT id, name, description, price FROM products WHERE id = ? LIMIT 0,1";
            $query = "SELECT id, name, description, price, image FROM products WHERE id = ? LIMIT 0,1";
            $stmt = $con->prepare( $query );
        
            // this is the first question mark
            $stmt->bindParam(1, $id);
        
            // execute our query
            $stmt->execute();
        
            // store retrieved row to a variable
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
            // values to fill up our form
            $name = $row['name'];
            $description = $row['description'];
            $price = $row['price'];
            $image = htmlspecialchars($row['image'], ENT_QUOTES);

            $response["status"] = "true";
            $response["message"] = "Product Details";
            $response["products"] = $row;

        } 
        // show error
        catch(PDOException $exception){
            $response["status"] = "false";
            $response["message"] = "No product(s) found!";
            die('ERROR: ' . $exception->getMessage());
        }

        
        echo json_encode($response);
        exit;

        ?>